'''Build up a command line parser which allows the user to specify:
1) how many datapoints to generate, and then
2) whether to plot with a filled in histogram or an outlined one, and
3) allow the user to specify the title of the plot

sample usage:
  bash-3.2$ python breakout10.py -t -n 200 -T "My Awesome Title"

Useful notes:
    https://docs.python.org/2/library/argparse.html

'''
from __future__ import (absolute_import, division, print_function,
                        unicode_literals)
import argparse
import matplotlib.pyplot as plt
import numpy as np


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=
                                     "App name: Breakout1 plotting app")

    # first item
    parser.add_argument('-t', action="store_true", dest="hist_outlined",
                        help="specify to plot filled or outlined histogram," +
                        " default is filled")
    parser.add_argument('-n', action="store", dest="data_points",
                        type=int,
                        help="specify the number of data points to use")
    parser.add_argument('-T', action="store", dest="title", type=str,
                        help="specify title of the plot")

    results = parser.parse_args()
    if results.data_points is not None:
        data_pts = results.data_points
    else:
        data_pts = 50

    if results.hist_outlined is not None and not results.hist_outlined:
        histtype = 'bar'
    else:
        # steps mean that the histogram are just outlines ...
        histtype = 'step'

    data = np.random.randn(data_pts)

    plt.hist(data, histtype=histtype)

    if results.title is not None:
        plt.title(results.title)

    plt.show()
