'''Build up a command line parser which allows the user to specify:
1) how many datapoints to generate, and then
2) whether to plot with a filled in histogram or an outlined one, and
3) allow the user to specify the title of the plot

sample usage:
  bash-3.2$ python breakout10.py -t -n 200 -T "My Awesome Title"

'''
