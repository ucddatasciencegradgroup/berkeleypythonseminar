from distutils.core import setup

setup(name="CalCalc",
      version='0.01',
      license="BSD",
      py_modules=['CalCalc'])
