"""2012 old homework 0 from Python Berkeley course
"""

from __future__ import (division, print_function)
# import urllib2
import argparse


def calculate(code):
    res = eval(code)
    print(res)
    return res


def test_calculate_multiplies_correctly():
    assert calculate('3. * 5.') == 15.


def test_calculate_divides_correctly():
    assert calculate('15. / 3.') == 5.


def test_calculate_adds_correctly():
    assert calculate('5 + 3.') == 8.


def test_calculate_subtracts_correctly():
    assert calculate('5 - 3.') == 2.


def test_calculate_does_modulo_correctly():
    assert calculate('3 % 2.') == 1.


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Name: Calculate stuff")
    parser.add_argument('-s', action="store", dest="code",
                        type=str, help="input your python code as string")

    res = parser.parse_args()
    if res.code is not None:
        calculate(res.code)
